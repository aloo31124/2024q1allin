import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.sass']
})
export class CockpitComponent implements OnInit {
  
  @Output() createRed = new EventEmitter<{name:string, content:string}>();
  @Output() createBlue = new EventEmitter<{name:string, content:string}>();
  newServerName = '';
  newServerContent = '';

  constructor() { }

  ngOnInit(): void {
  }

  addRed() {
    this.createRed.emit({
      name: this.newServerName,
      content: this.newServerContent
    });
  }

  addBlue() {
    this.createBlue.emit({
      name: this.newServerName,
      content: this.newServerContent
    });
  }
  

}
