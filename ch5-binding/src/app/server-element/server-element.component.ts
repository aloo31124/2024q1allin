import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.sass']
})
export class ServerElementComponent implements OnInit {

  //@Input() element: {name:string, type:string, content:string}
  @Input('srvElement') element: {name:string, type:string, content:string}

  constructor() { }

  ngOnInit(): void {
  }

}
