import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  serverElements = [{name:"server1", type:"red", content:"nothing"},
                    {name:"server2", type:"blue", content:"nothing 2!"}
                  ];
}
